Cave Mesh Generator
-------------------

This package allows you to create procedurally generated cave meshes. You can configure the generated caves by using the provided editor tool.

How to install:
- Just copy the CaveMeshGenerator folder to your Unity project's Assets folder

Usage:
- Open the "Cave Mesh Generator" editor from Window->Cave Mesh Generator
- Edit the map settings and the prefab name and then tap the "Save Prefab" button
- The generated prefab will be stored in the Assets/CaveMeshPrefabs folder.
- Drag the prefab to your scene
- That's it!
