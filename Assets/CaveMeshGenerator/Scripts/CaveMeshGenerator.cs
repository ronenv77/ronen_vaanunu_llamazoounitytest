﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CaveMeshGenerator
{
    #region ContouringCellNode
    public class ContouringCellNode
    {
        bool _isOn;
        Vector3 _vertexPosition;
        int _vertexIndex;

        ContouringCellNode _rightMidNode;
        ContouringCellNode _downMidNode;

        public ContouringCellNode(bool isOn)
        {
            _isOn = isOn;
            _vertexPosition = Vector3.zero;
            _vertexIndex = -1;
        }

        public bool IsOn
        {
            get
            {
                return _isOn;
            }
        }

        public Vector3 VertexPosition
        {
            get
            {
                return _vertexPosition;
            }
            set
            {
                _vertexPosition = value;
            }
        }

        public int VertexIndex
        {
            get
            {
                return _vertexIndex;
            }
            set
            {
                _vertexIndex = value;
            }
        }

        public ContouringCellNode RightMidNode
        {
            get
            {
                return _rightMidNode;
            }
            set
            {
                _rightMidNode = value;
            }
        }
        
        public ContouringCellNode DownMidNode
        {
            get
            {
                return _downMidNode;
            }
            set
            {
                _downMidNode = value;
            }
        }
    }
    #endregion // ContouringCellNode

    #region CellNodesTriangle
    public class CellNodesTriangle
    {
        List<ContouringCellNode> _nodes;

        public List<ContouringCellNode> Nodes
        {
            get
            {
                return _nodes;
            }
        }

        public CellNodesTriangle(ContouringCellNode node1, ContouringCellNode node2, ContouringCellNode node3)
        {
            _nodes = new List<ContouringCellNode>() { node1, node2, node3 };
        }
    }
    #endregion // CellNodesTriangle

    #region ContouringCell
    public class ContouringCell
    {
        ContouringCellNode _bottomLeftNode;
        ContouringCellNode _bottomRightNode;
        ContouringCellNode _topRightNode;
        ContouringCellNode _topLeftNode;

        ContouringCellNode _topMidNode;
        ContouringCellNode _rightMidNode;
        ContouringCellNode _bottomMidNode;
        ContouringCellNode _leftMidNode;

        List<CellNodesTriangle> _triangles;
        int _cellIndex = -1;

        public ContouringCell(ContouringCellNode bottomLeftNode, ContouringCellNode bottomRightNode, ContouringCellNode topRightNode, ContouringCellNode topLeftNode)
        {
            _bottomLeftNode = bottomLeftNode;
            _bottomRightNode= bottomRightNode;
            _topRightNode = topRightNode;
            _topLeftNode = topLeftNode;

            _topMidNode = _topLeftNode.RightMidNode;
            _rightMidNode = _topRightNode.DownMidNode;
            _bottomMidNode = _bottomLeftNode.RightMidNode;
            _leftMidNode = _topLeftNode.DownMidNode;

            _topMidNode.VertexPosition = (_topLeftNode.VertexPosition + _topRightNode.VertexPosition) * 0.5f;
            _rightMidNode.VertexPosition = (_topRightNode.VertexPosition + _bottomRightNode.VertexPosition) * 0.5f;
            _bottomMidNode.VertexPosition = (_bottomLeftNode.VertexPosition + _bottomRightNode.VertexPosition) * 0.5f;
            _leftMidNode.VertexPosition = (_topLeftNode.VertexPosition + _bottomLeftNode.VertexPosition) * 0.5f;

            CalculateContouringCellIndex();
            CalculateTriangles();
        }

        void CalculateContouringCellIndex()
        {
            int b0 = _bottomLeftNode.IsOn ? 1 : 0;
            int b1 = _bottomRightNode.IsOn ? 1 : 0;
            int b2 = _topRightNode.IsOn ? 1 : 0;
            int b3 = _topLeftNode.IsOn ? 1 : 0;
            _cellIndex = b0 + b1 * 2 + b2 * 4 + b3 * 8;
        }

        void AddTriangle(ContouringCellNode node1, ContouringCellNode node2, ContouringCellNode node3)
        {
            CellNodesTriangle triangle = new CellNodesTriangle(node1, node2, node3);
            _triangles.Add(triangle);
        }

        void CalculateTriangles()
        {
            _triangles = new List<CellNodesTriangle>();
            switch (_cellIndex)
            {
                case 0:
                    break;
                case 1:
                    AddTriangle(_bottomLeftNode, _bottomMidNode, _leftMidNode);
                    break;
                case 2:
                    AddTriangle(_bottomRightNode, _rightMidNode, _bottomMidNode);
                    break;
                case 3:
                    AddTriangle(_bottomLeftNode, _rightMidNode, _leftMidNode);
                    AddTriangle(_bottomRightNode, _rightMidNode, _bottomLeftNode);
                    break;
                case 4:
                    AddTriangle(_topRightNode, _topMidNode, _rightMidNode);
                    break;
                case 5:
                    AddTriangle(_bottomLeftNode, _topMidNode, _leftMidNode);
                    AddTriangle(_topMidNode, _bottomLeftNode, _topRightNode);
                    AddTriangle(_topRightNode, _bottomLeftNode, _rightMidNode);
                    AddTriangle(_rightMidNode, _topRightNode, _bottomLeftNode);
                    break;
                case 6:
                    AddTriangle(_topRightNode, _topMidNode, _bottomMidNode);
                    AddTriangle(_bottomRightNode, _topRightNode, _bottomMidNode);
                    break;
                case 7:
                    AddTriangle(_bottomLeftNode, _topMidNode, _leftMidNode);
                    AddTriangle(_topMidNode, _bottomLeftNode, _topRightNode);
                    AddTriangle(_topRightNode, _bottomLeftNode, _bottomRightNode);
                    break;
                case 8:
                    AddTriangle(_topLeftNode, _leftMidNode, _topMidNode);
                    break;
                case 9:
                    AddTriangle(_bottomLeftNode, _bottomMidNode, _topMidNode);
                    AddTriangle(_topLeftNode, _bottomLeftNode, _topMidNode);
                    break;
                case 10:
                    AddTriangle(_topLeftNode, _rightMidNode, _topMidNode);
                    AddTriangle(_rightMidNode, _topLeftNode, _bottomRightNode);
                    AddTriangle(_bottomRightNode, _topLeftNode, _bottomMidNode);
                    AddTriangle(_topLeftNode, _leftMidNode, _bottomMidNode);
                    break;
                case 11:
                    AddTriangle(_bottomLeftNode, _topMidNode, _topLeftNode);
                    AddTriangle(_bottomLeftNode, _rightMidNode, _topMidNode);
                    AddTriangle(_bottomLeftNode, _bottomRightNode, _rightMidNode);
                    break;
                case 12:
                    AddTriangle(_topLeftNode, _leftMidNode, _rightMidNode);
                    AddTriangle(_topRightNode, _topLeftNode, _rightMidNode);
                    break;
                case 13:
                    AddTriangle(_topLeftNode, _bottomLeftNode, _topRightNode);
                    AddTriangle(_topRightNode, _bottomLeftNode, _rightMidNode);
                    AddTriangle(_bottomLeftNode, _bottomMidNode, _rightMidNode);
                    break;
                case 14:
                    AddTriangle(_topLeftNode, _bottomRightNode, _topRightNode);
                    AddTriangle(_topLeftNode, _leftMidNode, _bottomRightNode);
                    AddTriangle(_bottomRightNode, _leftMidNode, _bottomMidNode);
                    break;
                case 15:
                    AddTriangle(_topLeftNode, _bottomLeftNode, _topRightNode);
                    AddTriangle(_topRightNode, _bottomLeftNode, _bottomRightNode);
                    break;
            }
        }

        public List<CellNodesTriangle> Triangles
        {
            get
            {
                return _triangles;
            }
        }
    }
    #endregion // ContouringCell

    public class CellNodesEdge
    {
        ContouringCellNode _node1;
        ContouringCellNode _node2;

        public CellNodesEdge(ContouringCellNode node1, ContouringCellNode node2)
        {
            _node1 = node1;
            _node2 = node2;
        }

        public ContouringCellNode Node1
        {
            get
            {
                return _node1;
            }
        }

        public ContouringCellNode Node2
        {
            get
            {
                return _node2;
            }
        }
    }

    #region CaveMesh
    public class CaveMesh
    {
        CaveGrid _grid;
        Mesh _mesh;

        Dictionary<ContouringCellNode, List<CellNodesTriangle>> _nodeTrianglesDictionary = new Dictionary<ContouringCellNode, List<CellNodesTriangle>>();

        public CaveGrid Grid
        {
            get
            {
                return _grid;
            }
        }

        public Mesh Mesh
        {
            get
            {
                return _mesh;
            }
        }

        bool IsOuterEdge(ContouringCellNode node1, ContouringCellNode node2)
        {
            int numOfCommonTriangles = 0;
            foreach (var triangle in _nodeTrianglesDictionary[node1])
            {
                if (_nodeTrianglesDictionary[node2].Contains(triangle))
                {
                    numOfCommonTriangles++;
                }
            }
            return numOfCommonTriangles == 1;
        }

        public void CreateMesh(int width, int height, float wallProbability, int numOfSimulationSteps, float wallHeight)
        {
            // Generate the grid
            _grid = CaveGrid.Generate(width, height, wallProbability, numOfSimulationSteps);

            // Build the mesh using the Marching Squares algorithm
            ContouringCellNode[,] contouringGridNodes = new ContouringCellNode[width, height];
            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    bool isOn = _grid[x, y].State == CellState.Wall;
                    ContouringCellNode node = new ContouringCellNode(isOn);
                    node.RightMidNode = new ContouringCellNode(false);
                    node.DownMidNode = new ContouringCellNode(false);
                    contouringGridNodes[x, y] = node;

                    float fx = (float)x - width * 0.5f;
                    float fy = (float)y - height * 0.5f;
                    node.VertexPosition = new Vector3(fx, 0.0f, fy);
                }
            }

            List<Vector3> verticesList = new List<Vector3>();
            List<int> trianglesList = new List<int>();
            int vertexIndex = 0;

            ContouringCell[,] contouringCellsGrid = new ContouringCell[width - 1, height - 1];
            for (int x = 0; x < contouringCellsGrid.GetLength(0); x++)
            {
                for (int y = 0; y < contouringCellsGrid.GetLength(1); y++)
                {
                    ContouringCellNode bottomLeftNode = contouringGridNodes[x, y + 1];
                    ContouringCellNode bottomRightNode = contouringGridNodes[x + 1, y + 1];
                    ContouringCellNode topRightNode = contouringGridNodes[x + 1, y];
                    ContouringCellNode topLeftNode = contouringGridNodes[x, y];

                    ContouringCell contouringCell = new ContouringCell(bottomLeftNode, bottomRightNode, topRightNode, topLeftNode);
                    contouringCellsGrid[x, y] = contouringCell;

                    foreach (CellNodesTriangle cellNodesTriangle in contouringCell.Triangles)
                    {
                       foreach (ContouringCellNode node in cellNodesTriangle.Nodes)
                       {
                           if (node.VertexIndex == -1)
                           {
                               verticesList.Add(node.VertexPosition);
                               node.VertexIndex = vertexIndex;
                               vertexIndex++;
                           }
                           trianglesList.Add(node.VertexIndex);

                           if (!_nodeTrianglesDictionary.ContainsKey(node))
                           {
                               _nodeTrianglesDictionary.Add(node, new List<CellNodesTriangle>());
                           }
                           _nodeTrianglesDictionary[node].Add(cellNodesTriangle);
                       }
                   }
                }
            }

            // Build the outer edges list
            HashSet<CellNodesEdge> outerEdges = new HashSet<CellNodesEdge>();
            for (int x = 0; x < contouringCellsGrid.GetLength(0); x++)
            {
                for (int y = 0; y < contouringCellsGrid.GetLength(1); y++)
                {
                    ContouringCell contouringCell = contouringCellsGrid[x, y];
                    foreach (CellNodesTriangle cellNodesTriangle in contouringCell.Triangles)
                    {
                        if (IsOuterEdge(cellNodesTriangle.Nodes[0], cellNodesTriangle.Nodes[1]))
                        {
                            outerEdges.Add(new CellNodesEdge(cellNodesTriangle.Nodes[0], cellNodesTriangle.Nodes[1]));
                        }
                        if (IsOuterEdge(cellNodesTriangle.Nodes[1], cellNodesTriangle.Nodes[2]))
                        {
                            outerEdges.Add(new CellNodesEdge(cellNodesTriangle.Nodes[1], cellNodesTriangle.Nodes[2]));
                        }
                        if (IsOuterEdge(cellNodesTriangle.Nodes[2], cellNodesTriangle.Nodes[0]))
                        {
                            outerEdges.Add(new CellNodesEdge(cellNodesTriangle.Nodes[2], cellNodesTriangle.Nodes[0]));
                        }
                    }
                }
            }

            vertexIndex = verticesList.Count;

            Vector3[] quadVertices = new Vector3[4];
            int[] quadIndices = new int[4]; 

            // Build the walls
            foreach (CellNodesEdge edge in outerEdges)
            {
                quadVertices[0] = edge.Node1.VertexPosition;
                quadVertices[1] = edge.Node1.VertexPosition - new Vector3(0.0f, wallHeight, 0.0f);
                quadVertices[2] = edge.Node2.VertexPosition - new Vector3(0.0f, wallHeight, 0.0f);
                quadVertices[3] = edge.Node2.VertexPosition;

                quadIndices[0] = edge.Node1.VertexIndex;
                quadIndices[1] = vertexIndex++;
                quadIndices[2] = vertexIndex++;
                quadIndices[3] = edge.Node2.VertexIndex;

                verticesList.Add(quadVertices[1]);
                verticesList.Add(quadVertices[2]);

                trianglesList.Add(quadIndices[0]);
                trianglesList.Add(quadIndices[1]);
                trianglesList.Add(quadIndices[2]);

                trianglesList.Add(quadIndices[0]);
                trianglesList.Add(quadIndices[2]);
                trianglesList.Add(quadIndices[3]);
            }

            _mesh = new Mesh();
            _mesh.vertices = verticesList.ToArray();
            _mesh.triangles = trianglesList.ToArray();
            _mesh.RecalculateNormals();
        }

        public static CaveMesh Generate(int width, int height, float wallProbability, int numOfSimulationSteps, float wallHeight)
        {
            CaveMesh caveMesh = new CaveMesh();
            caveMesh.CreateMesh(width, height, wallProbability, numOfSimulationSteps, wallHeight);
            return caveMesh;
        }
    }
    #endregion // CaveMesh
}
