using System.Collections.Generic;
using UnityEngine;

namespace CaveMeshGenerator
{
    public class CaveGridRoom
    {
        List<CaveGridCell> _cells;
        List<CaveGridCell> _edgeCells;

        public List<CaveGridCell> Cells
        {
            get
            {
                return _cells;
            }
        }

        public CaveGridRoom(List<CaveGridCell> otherRoomCells)
        {
            _cells = new List<CaveGridCell>(otherRoomCells);
        }

        public void CalculateEdgeCells(CaveGrid grid)
        {
            _edgeCells = new List<CaveGridCell>();
            foreach (CaveGridCell cell in _cells)
            {
                int numOfAdjacentCells = grid.GetNumOfAdjacentCellsWithState(cell.X, cell.Y, CellState.Wall);
                if (numOfAdjacentCells < 4)
                {
                    _edgeCells.Add(cell);
                    cell.IsEdgeCell = true;
                }
                else
                {
                    cell.IsEdgeCell = false;
                }
            }
        }

        public float GetDistanceToRoom(CaveGridRoom otherRoom, out CaveGridCell fromCell, out CaveGridCell toCell)
        {
            float minDistance = System.Single.MaxValue;
            fromCell = null;
            toCell = null;

            foreach (CaveGridCell edgeCell in _edgeCells)
            {
                foreach (CaveGridCell otherRoomEdgeCell in otherRoom._edgeCells)
                {
                    float dist = Vector2Int.Distance(new Vector2Int(edgeCell.X, edgeCell.Y), new Vector2Int(otherRoomEdgeCell.X, otherRoomEdgeCell.Y));
                    if (dist < minDistance)
                    {
                        minDistance = dist;
                        fromCell = edgeCell;
                        toCell = otherRoomEdgeCell;
                    }
                }
            }
            return minDistance;
        }

        public void ConnectRoom(CaveGridRoom otherRoom, CaveGridCell fromCell, CaveGridCell toCell, CaveGrid grid)
        {
            List<CaveGridCell> bridgeCells = new List<CaveGridCell>();
            List<Vector2Int> bridgeLinePoints = GetStraightLinePoints(fromCell.X, fromCell.Y, toCell.X, toCell.Y);
            foreach (Vector2Int point in bridgeLinePoints)
            {
                List<CaveGridCell> newCells = grid.GetCellsAroundPoint(point, 2);
                foreach (CaveGridCell newCell in newCells)
                {
                    newCell.State = CellState.Wall;
                    bridgeCells.Add(newCell);
                }
            }

            _cells.AddRange(otherRoom.Cells);
            _cells.AddRange(bridgeCells);

            CalculateEdgeCells(grid);
        }

        void SwapInt(ref int a, ref int b)
        {
            int tmp = a;
            a = b;
            b = tmp;
        }

        // Return the points lying between (x0, y0) to (x1, y1) using Bresenheim's line drawing algorithm
        List<Vector2Int> GetStraightLinePoints(int x0, int y0, int x1, int y1)
        {
            List<Vector2Int> points = new List<Vector2Int>();
            bool steep = Mathf.Abs(y1 - y0) > Mathf.Abs(x1 - x0);
            if (steep)
            {
                SwapInt(ref x0, ref y0);
                SwapInt(ref x1, ref y1);
            }
            if (x0 > x1)
            {
                SwapInt(ref x0, ref x1);
                SwapInt(ref y0, ref y1);
            }

            int dX = (x1 - x0);
            int dY = Mathf.Abs(y1 - y0);
            int err = (dX / 2);
            int yStep = (y0 < y1 ? 1 : -1);
            int y = y0;
 
            for (int x = x0; x <= x1; ++x)
            {
                if (steep)
                {
                    points.Add(new Vector2Int(y, x));
                }
                else
                {
                    points.Add(new Vector2Int(x, y));
                }

                err = err - dY;
                if (err < 0)
                {
                    y += yStep;
                    err += dX;
                }
            }
            return points;
        }

        public CaveGridRoom GetNearestRoom(List<CaveGridRoom> roomCandidates, out CaveGridCell firstCell, out CaveGridCell lastCell)
        {
            float minDist = System.Single.MaxValue;
            CaveGridRoom nearestRoom = null;
            firstCell = null;
            lastCell = null;

            foreach (CaveGridRoom room in roomCandidates)
            {
                if (room == this)
                {
                    continue;
                }

                CaveGridCell fromCell = null;
                CaveGridCell toCell = null;
                float dist = GetDistanceToRoom(room, out fromCell, out toCell);
                if (dist < minDist)
                {
                    minDist = dist;
                    nearestRoom = room;
                    firstCell = fromCell;
                    lastCell = toCell;
                }
            }
            return nearestRoom;
        }
    }
}
