namespace CaveMeshGenerator
{
    public enum CellState
    {
        Floor,
        Wall,
        Trap,
        Monster,
        Collectible,
        Water
    }

    public class CaveGridCell
    {
        CellState _state = CellState.Floor;
        int _x;
        int _y;
        bool _isEdgeCell;

        public CellState State
        {
            get
            {
                return _state;
            }
            set
            {
                _state = value;
            }
        }

        public int X
        {
            get
            {
                return _x;
            }
        }

        public int Y
        {
            get
            {
                return _y;
            }
        }

        public bool IsEdgeCell
        {
            get
            {
                return _isEdgeCell;
            }
            set
            {
                _isEdgeCell = value;
            }
        }

        public CaveGridCell(int x, int y)
        {
            _state = CellState.Floor;
            _x = x;
            _y = y;
            _isEdgeCell = false;
        }

        public CaveGridCell(CaveGridCell other)
        {
            this._state = other._state;
            _x = other.X;
            _y = other.Y;
            _isEdgeCell = other._isEdgeCell;
        }
    }
}