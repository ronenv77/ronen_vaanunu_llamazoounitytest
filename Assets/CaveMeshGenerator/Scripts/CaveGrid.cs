using System.Collections.Generic;
using UnityEngine;

namespace CaveMeshGenerator
{
   public class CaveGrid
    {
        CaveGridCell[,] _cells;
        List<CaveGridRoom> _rooms;

        // Overriding the [,] operator
        public CaveGridCell this[int x, int y]
        {
            get
            {
                return _cells[x, y];
            }
            set
            {
                _cells[x, y] = value;
            }
        }

        public int Width
        {
            get
            {
                return _cells.GetLength(0);
            }
        }

        public int Height
        {
            get
            {
                return _cells.GetLength(1);
            }
        }

        public List<CaveGridRoom> Rooms
        {
            get
            {
                return _rooms;
            }
        }

        private CaveGrid()
        {
        }

        // ctor
        private CaveGrid(int width, int height)
        {
            _cells = new CaveGridCell[width, height];
            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    _cells[x, y] = new CaveGridCell(x, y);
                }
            }
        }

        // copy ctor
        private CaveGrid(CaveGrid other)
        {
            _cells = new CaveGridCell[other.Width, other.Height];
            for (int x = 0; x < other.Width; x++)
            {
                for (int y = 0; y < other.Height; y++)
                {
                    _cells[x, y] = new CaveGridCell(other._cells[x, y]);
                }
            }
        }

        public bool IsCoordInGrid(int x, int y)
        {
            return (x >= 0 && x < Width && y >= 0 && y < Height);
        }

        public int GetNumOfSurroundingCellsWithState(int centerX, int centerY, CellState state)
        {
            int numOfSurroundingCells = 0;
            for (int x = centerX - 1; x <= centerX + 1; x++)
            {
                for (int y = centerY - 1; y <= centerY + 1; y++)
                {
                    if (x == centerX && y == centerY)
                    {
                        continue;
                    }

                    // Consider cells out of the grid as walls
                    if (!IsCoordInGrid(x,y) && state == CellState.Wall)
                    {
                        numOfSurroundingCells++;
                    }
                    else
                    if (IsCoordInGrid(x, y) && _cells[x, y].State == state)
                    {
                        numOfSurroundingCells++;
                    }
                }
            }
            return numOfSurroundingCells;
        }

        public int GetNumOfAdjacentCellsWithState(int centerX, int centerY, CellState state)
        {
            int numOfAdjacentsCells = 0;
            for (int x = centerX - 1; x <= centerX + 1; x++)
            {
                for (int y = centerY - 1; y <= centerY + 1; y++)
                {
                    if (!(x == centerX ^ y == centerY))
                    {
                        continue;
                    }

                    // Consider cells out of the grid as walls
                    if (!IsCoordInGrid(x, y) && state == CellState.Wall)
                    {
                        numOfAdjacentsCells++;
                    }
                    else
                    if (IsCoordInGrid(x, y) && _cells[x, y].State == state)
                    {
                        numOfAdjacentsCells++;
                    }
                }
            }
            return numOfAdjacentsCells;
        }

        public static CaveGrid Generate(int width, int height, float wallProbability, int numOfSimulationSteps)
        {
            CaveGrid randomizedGrid = new CaveGrid(width, height);
            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    randomizedGrid[x, y].State = Random.Range(0.0f, 1.0f) < wallProbability ? CellState.Wall : CellState.Floor;
                }
            }

            CaveGrid grid = randomizedGrid;
            for (int i = 0; i < numOfSimulationSteps; i++)
            {
                grid = RunSimulationStep(grid);
            }

            grid.CalculateRooms();
            grid.ConnectAllRooms();

            return grid;
        }

        static CaveGrid RunSimulationStep(CaveGrid origGrid)
        {
            CaveGrid grid = new CaveGrid(origGrid);
            for (int x = 0; x < origGrid.Width; x++)
            {
                for (int y = 0; y < origGrid.Height; y++)
                {
                    int numOfNeighbours = origGrid.GetNumOfSurroundingCellsWithState(x, y, CellState.Wall);
                    if (numOfNeighbours > 4)
                    {
                        grid[x, y].State = CellState.Wall;
                    }
                    else
                    if (numOfNeighbours < 4)
                    {
                        grid[x, y].State = CellState.Floor;
                    }
                }
            }
            return grid;
        }

        void CalculateRooms()
        {
            _rooms = new List<CaveGridRoom>();

            CaveGridCell[,] tmpGrid = new CaveGridCell[Width, Height];
            for (int x = 0; x < Width; x++)
            {
                for (int y = 0; y < Height; y++)
                {
                    tmpGrid[x, y] = new CaveGridCell(_cells[x, y]);
                }
            }

            for (int x = 0; x < Width; x++)
            {
                for (int y = 0; y < Height; y++)
                {
                    if (tmpGrid[x, y].State == CellState.Wall)
                    {
                        List<Vector2Int> roomCoords = ScanAndMarkRoomInGrid(tmpGrid, x, y);
                        List<CaveGridCell> roomCells = new List<CaveGridCell>();
                        foreach (Vector2Int coord in roomCoords)
                        {
                            roomCells.Add(_cells[coord.x, coord.y]);
                        }
                        CaveGridRoom room = new CaveGridRoom(roomCells);
                        room.CalculateEdgeCells(this);
                        _rooms.Add(room);
                    }
                }
            }
        }

        List<Vector2Int> ScanAndMarkRoomInGrid(CaveGridCell[,] grid, int startX, int startY)
        {
            List<Vector2Int> scannedCoords = new List<Vector2Int>();
            Queue<Vector2Int> queue = new Queue<Vector2Int>();
            queue.Enqueue(new Vector2Int(startX, startY));

            while (queue.Count > 0)
            {
                Vector2Int coord = queue.Dequeue();
                if (grid[coord.x, coord.y].State == CellState.Wall)
                {
                    scannedCoords.Add(coord);
                    grid[coord.x, coord.y].State = CellState.Floor;

                    // Scan surrounding cells
                    for (int x = coord.x - 1; x <= coord.x + 1; x++)
                    {
                        for (int y = coord.y - 1; y <= coord.y + 1; y++)
                        {
                            if (x == coord.x && y == coord.y)
                            {
                                continue;
                            }

                            if (x >= 0 && x < grid.GetLength(0) && y >= 0 && y < grid.GetLength(1) && grid[x, y].State == CellState.Wall)
                            {
                                queue.Enqueue(new Vector2Int(x, y));
                            }
                        }
                    }
                }
            }
            return scannedCoords;
        }

        void ConnectAllRooms()
        {
            Queue<CaveGridRoom> queue = new Queue<CaveGridRoom>();
            queue.Enqueue(_rooms[0]);
            while (queue.Count > 0)
            {
                CaveGridRoom room = queue.Dequeue();
                CaveGridCell firstCell = null;
                CaveGridCell lastCell = null;
                CaveGridRoom nearestRoom = room.GetNearestRoom(_rooms, out firstCell, out lastCell);
                if (nearestRoom != null)
                {
                    room.ConnectRoom(nearestRoom, firstCell, lastCell, this);
                    _rooms.Remove(nearestRoom);
                    queue.Enqueue(room);
                }
            }
        }

        public List<CaveGridCell> GetCellsAroundPoint(Vector2Int center, int radius)
        {
            List<CaveGridCell> cells = new List<CaveGridCell>();
            for (int x = -radius; x <= radius; x++)
            {
                for (int y = -radius; y <= radius; y++)
                {
                    if (x*x + y*y <= radius*radius)
                    {
                        int cellX = center.x + x;
                        int cellY = center.y + y;
                        if (IsCoordInGrid(cellX, cellY))
                        {
                            cells.Add(_cells[cellX, cellY]);
                        }
                    }
                }
            }
            return cells;
        }
    }
}
