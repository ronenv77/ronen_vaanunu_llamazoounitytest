﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace CaveMeshGenerator
{
    public class CaveMeshGeneratorEditorWindow : EditorWindow
    {
        int _mapWidth = 64;
        int _mapHeight = 64;
        float _wallProbability = 0.45f;
        float _wallHeight = 5.0f;
        int _numOfSimulationSteps = 5;
        string _prefabName = "CaveMesh";

        [MenuItem("Window/Cave Mesh Generator")]
        static void Init()
        {
            CaveMeshGeneratorEditorWindow window = (CaveMeshGeneratorEditorWindow)EditorWindow.GetWindow(typeof(CaveMeshGeneratorEditorWindow));
            window.titleContent.text = "Cave Mesh Generator";
            window.Show();
        }

        GameObject CreateCaveMeshGameObject(Mesh mesh, Material material)
        {
            GameObject go = new GameObject("CaveMesh", typeof(MeshRenderer), typeof(MeshFilter), typeof(MeshCollider));
            
            MeshFilter meshFilter = go.GetComponent<MeshFilter>();
            meshFilter.mesh = mesh;

            MeshRenderer meshRenderer = go.GetComponent<MeshRenderer>();
            meshRenderer.material = material;

            MeshCollider meshCollider = go.GetComponent<MeshCollider>();
            meshCollider.sharedMesh = mesh;

            return go;
        }

        void OnGUI()
        {
            GUILayout.Label("Map Settings", EditorStyles.boldLabel);
            _mapWidth = EditorGUILayout.IntField("Width", _mapWidth);
            _mapHeight = EditorGUILayout.IntField("Height", _mapHeight);
            _wallProbability = EditorGUILayout.FloatField("Wall Probability", _wallProbability);
            _wallHeight = EditorGUILayout.FloatField("Wall Height", _wallHeight);
            _numOfSimulationSteps = EditorGUILayout.IntField("Simulation Steps", _numOfSimulationSteps);

            EditorGUILayout.Space();

            _prefabName = EditorGUILayout.TextField("Prefab Name", _prefabName);

            if (GUILayout.Button("Save Prefab"))
            {
                if (!AssetDatabase.IsValidFolder("Assets/CaveMeshPrefabs"))
                {
                    AssetDatabase.CreateFolder("Assets", "CaveMeshPrefabs");
                }

                string meshPath = string.Format("Assets/CaveMeshPrefabs/{0}.mesh", _prefabName);
                CaveMesh caveMesh = CaveMeshGenerator.CaveMesh.Generate(_mapWidth, _mapHeight, _wallProbability, _numOfSimulationSteps, _wallHeight);                
                AssetDatabase.CreateAsset(caveMesh.Mesh, meshPath);

                string materialPath = string.Format("Assets/CaveMeshPrefabs/{0}.mat", _prefabName);
                Material material = new Material(Shader.Find("Diffuse"));
                AssetDatabase.CreateAsset(material, materialPath);
                
                string prefabPath = string.Format("Assets/CaveMeshPrefabs/{0}.prefab", _prefabName);
                GameObject caveMeshGameObject = CreateCaveMeshGameObject(caveMesh.Mesh, material);
                bool success;

                PrefabUtility.SaveAsPrefabAsset(caveMeshGameObject, prefabPath, out success);

                DestroyImmediate(caveMeshGameObject);

                AssetDatabase.SaveAssets();
                AssetDatabase.Refresh();
            }
        }
    }
}